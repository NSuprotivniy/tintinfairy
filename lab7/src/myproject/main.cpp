#include <iostream>
#include <string>
#include "LineNumberBuffer.hpp"


void coutbuf(string str) {
	linenumbuf buf(cout, 1024, "", "");
	ostream linenumput(&buf);
	//part1
	cout << "Original: " << str << endl;
	cout << "Written to buf: ";
	linenumput << str;
	linenumput.flush();
}
void cinbuff() {
	linenumbuf buf(cout, 1024, "", "");
	ostream linenumput(&buf);
	//part2
	char c;
	while ((c = getchar()) != EOF) {
		linenumput << c;
		linenumput.flush();
	}
}
int main() {

	string str1 = "In 4 bytes contains 32 bits\nUnix time starts from Jan 1, 1970\n";
	//part1
	coutbuf(str1);
	//part2
	cinbuff();
	
	system("pause");
	return 0;
}