#pragma once
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <cstring>

using namespace std;

class linenumbuf : public streambuf {
private:
	ostream * out;
	vector<char_type> buffer;
	string startb, endb;
	bool flag_up = 1;
	int num = 1;
protected:
	virtual int overflow(int c) override {
		if (out->good() && c != traits_type::eof()) {
			*pptr() = c; 
			pbump(1); 
			return sync() == 0 ? c : traits_type::eof();
		}

		return traits_type::eof();
	}

	virtual int sync() override {
		if (pptr() == pbase()) 
			return 0;
	
		char* flag;
		char* next;
		char* _pbase = pbase();
		ptrdiff_t sz = pptr() - pbase();
		ptrdiff_t nz;
		ptrdiff_t ssz = pptr() - pbase();
		startb = " "+to_string(num)+". ";
		flag = strchr(pbase(), '\n');

		
		if (flag_up) {
			*out << startb;
			flag_up = 0;
		}
		while ((flag = strchr(_pbase, '\n')) != NULL) {
			nz = flag - _pbase;
			out->write(_pbase, nz);
			_pbase = flag + 1;
			
			ssz -= nz + 1;
			num++;
			if (ssz == 0) {
				flag_up = 1;
				if((flag = strchr(_pbase, '\n')) != NULL)
				*out << std::endl << endb;
				else *out << std::endl;
			}
			else {
				endb = " " + to_string(num) + ". ";
				*out << std::endl << endb;
			}
			
			
		}

		out->write(_pbase, ssz);
		
		if (out->good()) {
			pbump(-sz); 
			return 0;
		}
		return -1;
	}

public:

	linenumbuf(ostream &_out, size_t _bufsize, string _startb, string _endb)
		: out(&_out), buffer(_bufsize), startb(_startb), endb(_endb)
	{
		char_type *buf = buffer.data();
		setp(buf, buf + (buffer.size() - 1)); 
	}
};




