#include "Iterator.hpp"
#include "SortSeq.hpp"
#include <iostream>
#include <string>
#include <vector>


int main() {

    std::cout << "step iterator" << std::endl;
    Iterator<char> it(std::cout, 3);
    it++ = '1';
    it++ = '2';
    it++ = '3';
    it++ = '4';
    it++ = '5';
    it++ = '6';

    std::cout << std::endl;
    std::cout << "sort sequence" << std::endl;

    std::vector<int> v{ 5, 7, 1, 9, 2, 4, 3, 8, 0 };
    merge_sort(v.begin(), v.end());

    for (auto a: v) {
        std::cout  << a << " ";
    }

    std::cout << std::endl;

    return 0;
}