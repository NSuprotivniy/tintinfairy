#include <gtest/gtest.h>
#include <iostream>
#include "../myproject/Guard.hpp"

struct ostream_param {
    std::ostream& stream;
    std::ios_base::fmtflags flag_before;
    char fill_before;
    std::ios_base::fmtflags flag_after;
    char fill_after;
    std::string text;
};

class Guard_test: public ::testing::TestWithParam<ostream_param> {};

TEST_P(Guard_test, _) {
    const ostream_param& param = GetParam();
    
    ostream& stream = param.stream;

    stream.flags(param.flag_before);
    stream.fill(param.fill_before);
    
    Guard *g = new Guard(stream);

    stream.flags(param.flag_after);
    stream.fill(param.fill_after);

    stream << param.text;

    delete g;
    EXPECT_EQ(param.flag_before, stream.flags());
    EXPECT_EQ(param.fill_before, stream.fill());
}

INSTANTIATE_TEST_CASE_P(
    _,
    Guard_test,
    ::testing::Values(
        ostream_param{
            std::cout,
            std::ios_base::hex,
            'a',
            std::ios_base::oct,
            'b',
            "hello"
        }
    )
);