#include <iostream> 
#include "Guard.hpp"
#include "Array.hpp"
#include <array>

int main() {
    std::array<std::array<int, 2>,3> sa1 = {{{1, 2}, {3, 4}, {5, 6}}};
    std::array<std::array<int, 2>,3> sa2 = {{{7, 8}, {9, 10}, {11, 12}}};
    Array<int, 3, 2> array1(sa1);
    Array<int, 3, 2> array2(sa2);

    Array<int, 3, 2> array3 = array1 | array2;

    for (size_t i = 0; i < 3; i++) 
        for (size_t j = 0; j < 2; j++)
            cout << array3(i, j) << endl;

    std::cout << (array1 == array2) << endl;
    
    return 0;
}