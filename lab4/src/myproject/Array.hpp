#pragma once
#include <array>
#include <iostream>

template <typename T, size_t n, size_t m> class Array;
template <typename T, size_t n, size_t m> class MaskedArray;

template <typename T, size_t n, size_t m>
class MaskedArray {
public:
    MaskedArray(std::array<std::array<T,m>,n> *array, Array<bool,n,m> mask);
    void operator =(T scalar);

private:
    std::array<std::array<T,m>,n> *array;
    Array<bool,n,m> mask;
};

template <typename T, size_t n, size_t m>
class Array {
public:
    Array(std::array<std::array<T,m>,n> array);
    Array();
    //~Array();
    Array<T,n,m> operator &(Array<T,n,m> array);
    Array<T,n,m> operator |(Array<T,n,m> array);
    Array<T,n,m> operator +(Array<T,n,m> array);
    Array<T,n,m> operator -(Array<T,n,m> array);
    T operator ()(int i, int j) const;
    MaskedArray<T,n,m> operator ()(Array<bool,n,m>);
    bool operator ==(const Array<T,n,m> rarray) const;
    
private:
    std::array<std::array<T,m>,n> array;
};


template <typename T, size_t n, size_t m>
Array<T,n,m>::Array(std::array<std::array<T,m>,n>  array) {
    this->array = array;
}

template <typename T, size_t n, size_t m>
Array<T,n,m>::Array() {
    this->array = std::array<std::array<T, m>,n>();   
}

// template <typename T, size_t n, size_t m>
// Array<T,n,m>::~Array() {
//     delete[] this->array;
// }

template <typename T, size_t n, size_t m>
Array<T,n,m> Array<T,n,m>::operator &(Array<T,n,m> rarray) {
    std::array<std::array<T,m>,n>  new_array;
    for (size_t i = 0; i < n; i++) {
        for (size_t j = 0; j < m; j++) {
            new_array[i][j] = this->array[i][j] & rarray(i,j);
        }
    }
    return Array(new_array);
}

template <typename T, size_t n, size_t m>
Array<T,n,m> Array<T,n,m>::operator |(Array<T,n,m> rarray) {
    std::array<std::array<T,m>,n>  new_array;
    for (size_t i = 0; i < n; i++) {
        for (size_t j = 0; j < m; j++) {
            new_array[i][j] = this->array[i][j] | rarray(i,j);
        }
    }
    return Array(new_array);
}

template <typename T, size_t n, size_t m>
Array<T,n,m> Array<T,n,m>::operator  +(Array<T,n,m> rarray) {
    std::array<std::array<T,m>,n>  new_array;
    for (size_t i = 0; i < n; i++) {
        for (size_t j = 0; j < m; j++) {
            new_array[i][j] = this->array[i][j] + rarray(i,j);
        }
    }
    return Array(new_array);
}

template <typename T, size_t n, size_t m>
Array<T,n,m> Array<T,n,m>::operator  -(Array<T,n,m> rarray) {
    std::array<std::array<T,m>,n>  new_array;
    for (size_t i = 0; i < n; i++) {
        for (size_t j = 0; j < m; j++) {
            new_array[i][j] = this->array[i][j] - rarray(i,j);
        }
    }
    return Array(new_array);
}

template <typename T, size_t n, size_t m>
T Array<T,n,m>::operator()(int i, int j) const {
    return this->array[i][j];
}

template <typename T, size_t n, size_t m>
bool Array<T,n,m>::operator ==(const Array<T,n,m> rarray) const {
    for (size_t i = 0; i < n; i++) {
        for (size_t j = 0; j < m; j++) {
            if ((*this)(i, j) != rarray(i,j)) {
                return false;
            }
        }
    }
    return true;
}

template <typename T, size_t n, size_t m>
MaskedArray<T,n,m> Array<T,n,m>::operator ()(Array<bool,n,m> mask) {
    return MaskedArray<T,n,m>(&(this->array), mask);
}

template <typename T, size_t n, size_t m>
MaskedArray<T,n,m>::MaskedArray(std::array<std::array<T,m>,n> *array, Array<bool,n,m> mask) {
    this->array = array;
    this->mask = mask;
}

template <typename T, size_t n, size_t m>
void MaskedArray<T,n,m>::operator =(T scalar) {
    for (size_t i = 0; i < n; i++) {
        for (size_t j = 0; j < n; j++) {
            if (this->mask(i, j) == true) {
                (*(this->array))[i][j] = scalar;
            }
        }
    }
}