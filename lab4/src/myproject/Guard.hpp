#pragma once

#include <iostream>
#include <memory>
using namespace std;

class Guard {
public:
    Guard(ostream &stream);
    ~Guard();
    ostream & getStream();
    void restoreState();

private:
    ostream *stream;
    ios_base::fmtflags flags;
    char fill;
};