#include <iostream>
#include <thread.hpp>
#include <chrono>
using namespace tintinfairy;
 
int f1(void*) {
    std::cout << "abcd" << std::endl;
    return 0;
}

int f2(void*) {
    std::cout << "1234" << std::endl;
    return 0;
}
 
int main() {
    thread *thread_1 = new thread(f1);
    thread *thread_2 = new thread(f2);

    std::cout << thread_1->get_id() << std::endl;
    std::cout << thread_2->get_id() << std::endl;

    std::cout << "thread_1 " << (thread_1->joinable() ? "true" : "false") << std::endl;
    std::cout << "thread_2 " << (thread_2->joinable() ? "true" : "false") << std::endl;

    std::cout << "swap" << std::endl;

    thread_1->swap(*thread_2);

    std::cout << "thread_1->join();" << std::endl;
    thread_1->join();

    std::cout << "thread_1 " << (thread_1->joinable() ? "true" : "false") << std::endl;
    std::cout << "thread_2 " << (thread_2->joinable() ? "true" : "false") << std::endl;

    std::cout << "thread_2->join();" << std::endl;
    thread_2->join();

    std::cout << "thread_1 " << (thread_1->joinable() ? "true" : "false") << std::endl;
    std::cout << "thread_2 " << (thread_2->joinable() ? "true" : "false") << std::endl;

    delete thread_1;
    delete thread_2;
}