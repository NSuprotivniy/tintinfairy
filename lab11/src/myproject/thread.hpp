#pragma once

#include <cstddef>
#include <utility>
#include <sched.h> 
#include <sys/wait.h>
#include <signal.h> 
#include <exception>


namespace tintinfairy {

    class thread_exception: public std::exception {
    public:
        thread_exception(const char *m) : message(m), std::exception() {};
        virtual const char* what() const throw() {
            return message;
        }
    private:
        const char *message;
    };

    enum {
        stack_capacity = 8388608 // 8MB
    };

    class thread {
    public:
        thread(thread&& other);    
        thread(int(*fun)(void*));
        ~thread();    
        bool joinable() { return id != 0; };
        int get_id() { return id; }
        void join();
        void swap(thread & other);

    private:
        int id = NULL;
        int flags = CLONE_VM | SIGCHLD;
        char *stack;
    };
    
    thread::thread(thread&& other) {
        swap(other);
    }
    
    thread::thread( int(*f) (void*) ) {
        stack = new char[stack_capacity];
        id = clone(f, stack + stack_capacity, this->flags, nullptr);
        if (id == -1) { throw thread_exception("thread clone error");}
    }

    thread::~thread() {
        if (id != NULL) { 
            if (kill(id, SIGTERM) == -1) { throw thread_exception("thread kill error");}
        }
        delete[] stack;
    };

    void thread::join() {
        if (waitpid(id, NULL, 0) == -1) { throw thread_exception("thread wait error");}
        id = NULL;
    }

    void thread::swap(thread & other) {
        std::swap(this->id, other.id);
        std::swap(this->flags, other.flags);
        std::swap(this->stack, other.stack);
    }
}