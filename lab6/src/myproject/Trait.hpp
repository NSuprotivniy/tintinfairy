#pragma once
// part 3
#ifndef SIZE_HH
#define SIZE_HH

#include "Account.hpp"
#include "Group.hpp"
#include <unordered_set>


	template <class T>
	struct trait;

	template <>
	struct trait<Account>
	{
		typedef size_t result_type;
		static result_type size(Account &account)
		{
			return account.all_data().size() * sizeof(char) + sizeof(int);
		}

	};

	template <>
	struct trait<Group>
	{
		typedef size_t result_type;
		static result_type size(Group &group)
		{
			size_t sum = NULL;
			for (auto s : group.get_members())
			{
				sum += trait<Account>::size(s);
			}

			sum += sizeof(size_t) + group.get_groupname().size() * sizeof(char);
			return sum;
		}
	};

#endif