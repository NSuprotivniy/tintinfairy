#include <gtest/gtest.h>
#include "../myproject/message.hpp"
#include <sstream>
#include <iostream>
using namespace std;

TEST(Max, Compare) {
    std::stringstream ss;
    message(ss, "%s, %s. %d + %d = %d", "hello", "world", 2, 2, 4);
    
    EXPECT_EQ("hello, world. 2 + 2 = 4", ss.str());
}