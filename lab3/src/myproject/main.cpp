#include "message.hpp"
#include <sstream>
#include <iostream>
#include <regex>

using namespace std;

int main() {
    std::stringstream ss;
    message(ss, "%f", 10.01);
    cout << ss.str() << endl;
    return 0;
}