#pragma once

#include <stdlib.h>
#include <stdarg.h>
#include <unistd.h>
#include <limits.h>
#include <stdio.h>
#include <ostream>
#include <string>
#include <regex>
#include <iostream>
#include <string>
#include <sstream>

using namespace std;

template<typename T, typename... Targs>
void message(ostream& out, const char *formats, T value, Targs... Fargs);


void apply_format(stringstream &ss, string &format) {
	regex e("(\\d*).?(\\d*)");
	smatch m;
	regex_search(format, m, e);
 	if (m[1] != "") {ss.width(std::stoi(m[1]));}
	if (m[2] != "") {ss << std::fixed; ss.precision(std::stoi(m[2]));}
}


template<typename T>
string parse_char(string &modifier, string &format, T &value) {
	stringstream ss;
	ss << value;
	return ss.str();
}

template<typename T>
string parse_decimal(string &modifier, string &format, T &value) {
	stringstream ss;
	apply_format(ss, format);
	ss << value;
	return ss.str();
}

template<typename T>
string parse_exponential(string &modifier, string &format, T &value) {
	stringstream ss;
	if (format == "E") {ss << std::uppercase;}
	ss << ios::scientific << value;
	return ss.str();
}

template<typename T>
string parse_float(string &modifier, string &format, T &value) {
	stringstream ss;
	apply_format(ss, format);
	ss << value;
	return ss.str();
}

template<typename T>
string parse_float_or_exp(string &modifier, string &format, T &value) {
	stringstream ss;
	ss << ios::scientific << value;
	return ss.str();
}

template<typename T>
string parse_oct(string &modifier, string &format, T &value) {
	stringstream ss;
	ss << ios::oct << value;
	return ss.str();
}

template<typename T>
string parse_string(string &modifier, string &format, T &value) {
	stringstream ss;
	ss << value;
	return ss.str();
}

template<typename T>
string parse_unsigned(string &modifier, string &format, T &value) {
	stringstream ss;
	apply_format(ss, format);
	ss << value;
	return ss.str();
}

template<typename T>
string parse_hex(string &modifier, string &format, T &value) {
	stringstream ss;
	if (format == "X") {ss << std::uppercase;}
	ss << ios::hex << value;
	return ss.str();
}

template<typename T>
string parse(string &modifier, string &format, T &value) {
	stringstream ss;
	if (modifier == "c") { return parse_char(modifier, format, value); }
	else if (modifier == "d") { return parse_decimal(modifier, format, value); }
	else if (modifier == "i") { return parse_decimal(modifier, format, value); }
	else if (modifier == "e") { return parse_exponential(modifier, format, value); }
	else if (modifier == "E") { return parse_exponential(modifier, format, value); }
	else if (modifier == "f") { return parse_float(modifier, format, value); }
	else if (modifier == "g") { return parse_float_or_exp(modifier, format, value); }
	else if (modifier == "G") { return parse_float_or_exp(modifier, format, value); }
	else if (modifier == "o") { return parse_oct(modifier, format, value); }
	else if (modifier == "s") { return parse_string(modifier, format, value); }
	else if (modifier == "u") { return parse_unsigned(modifier, format, value); }
	else if (modifier == "x") { return parse_hex(modifier, format, value); }
	else if (modifier == "X") { return parse_hex(modifier, format, value); }
}

void message(ostream& out, const char *formats) {
	out << formats;
} 

template<typename T, typename... Targs>
void message(ostream& out, const char *formats, T value, Targs... Fargs) {
    regex e("%(\\d*.?\\d*)([cdieEfgGosuxXpn])");
    smatch m;
	string f(formats);
    if (std::regex_search (f,m,e)) {
        string format = m[1];
        string modifier = m[2];
		string replacement = parse(modifier, format, value);
        out << m.prefix().str() << replacement;
        f = m.suffix().str();

		message(out, f.c_str(), Fargs...);
    } else {
		out << f;
	}
} 