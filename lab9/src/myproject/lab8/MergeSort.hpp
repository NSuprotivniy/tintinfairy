#pragma once
#include <iterator>
#include <vector>
#include <algorithm>
#include <thread>
#include <math.h>


using namespace std;
using namespace std::chrono;

unsigned int num_threads = std::thread::hardware_concurrency();
unsigned int max_depth = floor(log(num_threads) / log(2));

void threaded_merge_sort(std::vector<int>::iterator first, std::vector<int>::iterator last, int depth)
{
	if (last - first > 1) {

		auto middle = first + (last - first) / 2;

		if (depth < max_depth) {
			thread t1 = thread(threaded_merge_sort, first, middle, depth + 1);
			thread t2 = thread(threaded_merge_sort, middle, last, depth + 1);
			t1.join();
			t2.join();
		} else {
			threaded_merge_sort(first, middle, depth + 1);
			threaded_merge_sort(middle, last, depth + 1);
		}
		std::inplace_merge(first, middle, last);
	}
}

void nonthread_merge_sort(std::vector<int>::iterator first, std::vector<int>::iterator last)
{
	if (last - first > 1) {

		auto middle = first + (last - first) / 2;

		nonthread_merge_sort(first, middle);
		nonthread_merge_sort(middle, last);
		
		std::inplace_merge(first, middle, last);
	}
}

void multi_merge_sort(std::vector<int> &vec1) {
	auto middle = vec1.begin() + (vec1.end() - vec1.begin()) / 2;
	
	thread t1 = thread(threaded_merge_sort, vec1.begin(), middle, 1);
	thread t2 = thread(threaded_merge_sort, middle, vec1.end(), 1);
	t1.join();
	t2.join();

	std::inplace_merge(vec1.begin(), middle, vec1.end());	
}
void print_vec(std::vector<int>vec) {

	for (auto e : vec) {
		std::cout << e << " ";
	}
	std::cout << std::endl;
}
void lin_merge_sort(std::vector<int> &vec)
{
	nonthread_merge_sort(vec.begin(), vec.end());
}
bool correct(std::vector<int> vec1, std::vector<int> vec2) {
	bool flag = 0;
	for (int i = 0; i < vec1.size(); i++) {
		if (vec1[i] == vec2[i]) flag = 1;
		else flag = 0;
	}
	return flag;
}
milliseconds threaded_work_time(std::vector<int> &vec) {

	auto start = system_clock::now();
	multi_merge_sort(vec);
	auto end = system_clock::now();
	milliseconds elapsed = duration_cast<milliseconds>(end - start);
	return elapsed;
}

milliseconds nonthreaded_work_time(std::vector<int> &vec) {

	auto start = system_clock::now();
	lin_merge_sort(vec);
	auto end = system_clock::now();
	milliseconds elapsed = duration_cast<milliseconds>(end - start);
	return elapsed;
}

