#pragma once
#include <ostream>
#include <iostream>

template <typename T>
class Iterator {
public:
    Iterator(std::basic_ostream<T>& stream, const int step);
    Iterator(std::basic_ostream<T>& stream, const int step, const int current_step);
    Iterator& operator=( const T& value );
    Iterator& operator*();
    Iterator& operator++();
    Iterator& operator++( int );

private:
    std::basic_ostream<T> *stream;
    int step;
    int current_step;
};

template <typename T>
Iterator<T>::Iterator(std::basic_ostream<T>& stream, const int step) {
    this->stream = &stream;
    this->step = step;
    this->current_step = 0;
}

template <typename T>
Iterator<T>::Iterator(std::basic_ostream<T>& stream, const int step, const int current_step) {
    this->stream = &stream;
    this->step = step;
    this->current_step = current_step;
}

template <typename T>
Iterator<T>& Iterator<T>::operator=( const T& value ) {
    if (current_step == 0) {
        *(stream) << value;
    } 
}

template <typename T>
Iterator<T>& Iterator<T>::operator*() {
    return *this;
}

template <typename T>
Iterator<T>& Iterator<T>::operator++() {
    current_step = (current_step + 1) % step;
    return *this;
}

template <typename T>
Iterator<T>& Iterator<T>::operator++( int ) {
    Iterator<T> it = *this;
    current_step = (current_step + 1) % step;
    return *it;
}