#pragma once

#include <stdlib.h>
#include <stdarg.h>
#include <unistd.h>
#include <limits.h>
#include <stdio.h>
#include <ostream>
#include <string>

using namespace std;

void message(ostream &out, char *format, ...);
string parse(string &modifier, string &format, va_list &args_list);
string parse_char(string &modifier, string &format, va_list &args_list);
string parse_decimal(string &modifier, string &format, va_list &args_list);
string parse_exponential(string &modifier, string &format, va_list &args_list);
string parse_float_or_exp(string &modifier, string &format, va_list &args_list);
string parse_float(string &modifier, string &format, va_list &args_list);
string parse_oct(string &modifier, string &format, va_list &args_list);
string parse_string(string &modifier, string &format, va_list &args_list);
string parse_unsigned(string &modifier, string &format, va_list &args_list);
string parse_hex(string &modifier, string &format, va_list &args_list);