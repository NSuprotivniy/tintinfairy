#include "message.hpp"
#include <regex>
#include <iostream>
#include <string>
#include <sstream>

void message(ostream& out, char *formats, ...) {
    va_list args_list;
	va_start(args_list, formats);

    regex e("%(\\d*.?\\d*)([cdieEfgGosuxXpn])");
    smatch m;
	string f(formats);
    while (std::regex_search (f,m,e)) {
        string format = m[1];
        string modifier = m[2];
		string replacement = parse(modifier, format, args_list);
        out << m.prefix().str() << replacement;
        f = m.suffix().str();
    }
	va_end(args_list);
	out << f;
} 

string parse(string &modifier, string &format, va_list &args_list) {
	stringstream ss;
	if (modifier == "c") { return parse_char(modifier, format, args_list); }
	else if (modifier == "d") { return parse_decimal(modifier, format, args_list); }
	else if (modifier == "i") { return parse_decimal(modifier, format, args_list); }
	else if (modifier == "e") { return parse_exponential(modifier, format, args_list); }
	else if (modifier == "E") { return parse_exponential(modifier, format, args_list); }
	else if (modifier == "f") { return parse_float(modifier, format, args_list); }
	else if (modifier == "g") { return parse_float_or_exp(modifier, format, args_list); }
	else if (modifier == "G") { return parse_float_or_exp(modifier, format, args_list); }
	else if (modifier == "o") { return parse_oct(modifier, format, args_list); }
	else if (modifier == "s") { return parse_string(modifier, format, args_list); }
	else if (modifier == "u") { return parse_unsigned(modifier, format, args_list); }
	else if (modifier == "x") { return parse_hex(modifier, format, args_list); }
	else if (modifier == "X") { return parse_hex(modifier, format, args_list); }
}

void apply_format(stringstream &ss, string &format);

string parse_char(string &modifier, string &format, va_list &args_list) {
	stringstream ss;
	char a = va_arg(args_list, char);
	ss << a;
	return ss.str();
}

string parse_decimal(string &modifier, string &format, va_list &args_list) {
	stringstream ss;
	apply_format(ss, format);
	int a = va_arg(args_list, int);
	ss << a;
	return ss.str();
}

string parse_exponential(string &modifier, string &format, va_list &args_list) {
	stringstream ss;
	double a = va_arg(args_list, double);
	ss << ios::scientific << a;
	return ss.str();
}

string parse_float(string &modifier, string &format, va_list &args_list) {
	stringstream ss;
	apply_format(ss, format);
	double a = va_arg(args_list, double);
	ss << a;
	return ss.str();
}

string parse_float_or_exp(string &modifier, string &format, va_list &args_list) {
	stringstream ss;
	double a = va_arg(args_list, double);
	ss << ios::scientific << a;
	return ss.str();
}

string parse_oct(string &modifier, string &format, va_list &args_list) {
	stringstream ss;
	int a = va_arg(args_list, int);
	ss << ios::oct << a;
	return ss.str();
}

string parse_string(string &modifier, string &format, va_list &args_list) {
	stringstream ss;
	char * a = va_arg(args_list, char *);
	ss << a;
	return ss.str();
}

string parse_unsigned(string &modifier, string &format, va_list &args_list) {
	stringstream ss;
	apply_format(ss, format);
	unsigned int a = va_arg(args_list, unsigned int);
	ss << a;
	return ss.str();
}

string parse_hex(string &modifier, string &format, va_list &args_list) {
	stringstream ss;
	if (format == "X") {ss << std::uppercase;}
	int a = va_arg(args_list, int);
	ss << ios::hex << a;
	return ss.str();
}

void apply_format(stringstream &ss, string &format) {
	regex e("(\\d*).?(\\d*)");
	smatch m;
	regex_search(format, m, e);
 	if (m[1] != "") {ss.width(std::stoi(m[1]));}
	if (m[2] != "") {ss << std::fixed; ss.precision(std::stoi(m[2]));}
}
