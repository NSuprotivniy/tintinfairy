#include "Guard.hpp"

Guard::Guard(ostream &stream) {
    this->stream = &stream;
    this->flags = stream.flags();
    this->fill = stream.fill();
}

ostream & Guard::getStream() {
    return *(this->stream);
}

void Guard::restoreState() {
    this->stream->flags(this->flags);
    this->stream->fill(this->fill);
}

Guard::~Guard() {
    this->restoreState();
}

