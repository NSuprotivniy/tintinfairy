#pragma once
#include <iostream>
#include <string>
#include <unordered_set>

struct AccountHasher
{
	std::size_t operator()(const Account& k) const
	{
		using std::size_t;
		using std::hash;
		using std::string;

		return ((hash<int>()(k.id)
			^ (hash<string>()(k.name) << 1)) >> 1)
			^ (hash<string>()(k.surname) << 1);
	}
};
class Group {

public:
	int id;
	std::string groupname;
	std::unordered_set <Account, AccountHasher> members_set;
	void set_members(std::unordered_set <Account, AccountHasher> set) { members_set = set; }
	std::unordered_set <Account, AccountHasher> get_members() {
		return members_set;
	}
	std::string get_groupname() {
		return groupname;
	}
};