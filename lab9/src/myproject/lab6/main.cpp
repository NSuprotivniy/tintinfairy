#include <iostream>
#include "Account.hpp"
#include "Group.hpp"
#include "Trait.hpp"
#include <iostream>
#include <string>
#include <unordered_map>
#include <unordered_set>
//using namespace std;


int main() {
	//part 1
	Account account;
	Group group;
	trait<Account> actrait;
	trait<Group> grtrait;

	account.set_data(2, "Kseniya", "Kudrinskaya");
	account.print_data();

	std::unordered_map<Account, std::string, AccountHasher> mymap;
	mymap = { { { 13, "Kseniya", "Kudrinskaya" }, "edj2j4b" },{ { 21, "Mary", "Jane" }, "adjh21hfj" } };

	std::unordered_set<Account, AccountHasher> myset;
	myset.insert({ 1, "John" , "Smith" });
	myset.insert({ 2, "Helena", "Red" });
	for (Account st : myset)
		st.print_data();

	//part 2
	group.set_members(myset);

	//part 3
	std::cout<<actrait.size(account)<<std::endl;
	std::cout<<grtrait.size(group)<<std::endl;
	return 0;
}