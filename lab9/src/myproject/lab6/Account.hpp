#pragma once
#include <iostream>
#include <string>
class Account {

public:
	int id;
	std::string name;
	std::string surname;
	void set_data(int that_id, std::string that_name, std::string that_surname) {
		id = that_id;
		name = that_name;
		surname = that_surname;
	}
	void print_data() {
		std::cout << id << " " << name << " " << surname << std::endl;
	}
	bool operator==(const Account &other) const
	{
		return (id == other.id
			&& name == other.name
			&& surname == other.surname);
	}
	const std::string all_data() {
		std::string result = "";
		result = name + surname + std::to_string(id);
		return result;
	}
};