#pragma once

#include <iostream>
#include <utility>
#include <cstring>

namespace newns {

    template <class T> class Iterator;


    // ---------------
    // CONTAINER CLASS
    //
    template <class T> class vector {
    public:

        // Defining types:

        typedef Iterator <T>                iterator;
        typedef T                           value_type;
        typedef T       &                   reference;
        typedef std::size_t                         size_type;
        typedef std::ptrdiff_t                      difference_type;

    private:
        size_type vector_size;
        T *entries;
        size_type _capacity;
        const size_type initial_capacity = 16;
        void init();

    public:

        // Allocation and Deallocation:

        vector ( const vector<T> & other );     
        vector ( size_type num, const T & value = T() );
        vector();
        ~vector();

        T *data();
        int capacity();

        vector<T> & operator = ( vector<T> && other ) noexcept;        // Move operator
        vector<T> & operator = ( const vector<T> & other );            // Copy operator

        void swap   ( vector<T> & other );
        void assign ( size_type count, const T & value = T() );
        void assign ( iterator start, iterator end );

        // Access resources:

        iterator               begin();                  // returns iterator to the first data member
        iterator               end();                    // returns iterator to element after the last data member

        bool empty()            const;                   // true - container empty | false - container not empty
        size_type size()        const;					 // returns number of elements
        reference front();
        reference back();
        bool contains(T data);

        // Inserting and Removing:

        void push_back     ( T data );            // put data
        void clear();                                     // clears all contained data
        void pop_back();                                  // delete last element of the container

        // Some other operators:

        bool operator == ( vector<T> & other );
        bool operator != ( vector<T> & other );
        bool operator <  ( vector<T> & other );    
        bool operator >  ( vector<T> & other );    
        bool operator <= ( vector<T> & other );    
        bool operator >= ( vector<T> & other );    
    };

    // 
    // CONTAINER CLASS
    // ---------------


    // -----------------
    // CONTAINER METHODS
    // 

    template <class T> void vector<T>::init() {
        this->_capacity = this->initial_capacity;
        this->entries = new T[this->initial_capacity];
        this->vector_size = 0;
    }

    template <class T> vector<T>::vector() {
        init();
    }

    template <class T> vector<T>::vector(const vector<T> & other) {
        init();
        for (auto it : other) { this->push_back(it); }
    }

    template <class T> vector<T>::vector(size_type num, const T & value) {
        init();
        for (size_type i = 0; i < num; i++) { this->push_back(value); }
    }

    template <class T> vector<T>::~vector() {
        delete[] entries;
    }

    template <class T> typename vector<T>::size_type vector<T>::size() const {
        return vector_size;
    }

    template <class T> bool vector<T>::empty() const { 
        return !vector_size; 
    }

    template <class T> typename vector<T>::reference vector<T>::front() {
        return entries[0];
    }

    template <class T> typename vector<T>::reference vector<T>::back() {
        if (vector_size == 0) { return entries[0]; }
        else { return entries[vector_size - 1]; }
    }

    template <class T> void vector<T>::assign(size_type count, const T & value) {
        for (size_type i = 0; i < count; i++) { this->push_back(value); }
    }

    template <class T> void vector<T>::assign(iterator start, iterator end) {
        while (start != end) { this->push_back(*start++); }
    }

    template <class T> void vector<T>::pop_back() {
        if (vector_size == 0) { return entries[0]; }
        if ((_capacity - 1 > initial_capacity >> 2) && (vector_size - 1 > _capacity >> 2)) {
            _capacity <<= 1;
            T *new_entries = new T[_capacity];
            memcpy(new_entries, entries, vector_size);
            delete[] entries;
            entries = new_entries;
        }
        else { return entries[--vector_size]; }
    }

    template <class T> void vector<T>::push_back(T data) {
        if (vector_size + 1 > _capacity >> 1) {
            _capacity <<= 1;
            T *new_entries = new T[_capacity];
            memcpy(new_entries, entries, vector_size * sizeof(T));
            delete[] entries;
            entries = new_entries;
        }
        entries[vector_size++] = data;
    }

    template <class T> void vector<T>::swap(vector<T> & other) {
        std::swap<vector<T>>((*this), other);
    }

    template <class T> void vector<T>::clear() {
        delete[] entries;
        init();
    }

    template <class T> bool vector<T>::contains(T data) {
        for (int i = 0; i < vector_size; i++) {
            if (entries[i] == data) { return true; }
        }
    }

    template <class T> typename vector<T>::iterator vector<T>::begin() {
        return iterator(entries, entries + vector_size - 1, entries);
    }

    template <class T> typename vector<T>::iterator vector<T>::end() { 
        return iterator(entries, entries + vector_size - 1, entries + vector_size - 1);
    }

    template <class T> T *vector<T>::data() {
        return entries;
    }

    template <class T> int vector<T>::capacity() {
        return _capacity;
    }

    template <class T> vector<T> & vector<T>::operator = (vector<T> && other) noexcept {
        delete entries;
        this->_capacity = other._capacity;
        this->size = other.size;
        this->entries = memcpy(this->entries, other.data, this->_capacity);
        return *this;    
    }

    template <class T> vector<T> & vector<T>::operator = (const vector<T> & other) {
        delete entries;
        this->_capacity = other._capacity;
        this->size = other.size;
        this->entries = memcpy(this->entries, other.data, this->_capacity);
        return *this;  
    }

    template <class T> bool vector<T>::operator == (vector<T> & other) {
        if (this->size() != other.size()) { return false; }
        else {
            auto me = this->begin(), oth = other.begin();
            auto __end = this->end();
            while (me != __end) {
                if (*me++ != *oth++) { return false; }
            }
            return true;
        }
    }

    template <class T> bool vector<T>::operator != (vector<T> & other) {
        return !((*this) == other);
    }

    template <class T> bool vector<T>::operator <  (vector<T> & other) {
        if (this->size() != other.size()) { return false; }
        else {
            auto me = this->begin(), oth = other.begin();
            auto __end = this->end();
            while (me !=__end) {
                if (*me++ >= *oth++) { return false; }
            }
            return true;
        }
    }

    template <class T> bool vector<T>::operator >  (vector<T> & other) {
        if (this->size() != other.size()) { return false; }
        else {
            auto me = this->begin(), oth = other.begin();
            auto __end = this->end();
            while (me != __end) {
                if (*me <= *oth) { return false; }
            }
            return true;
        }
    }

    template <class T> bool vector<T>::operator <= (vector<T> & other) {
        return !((*this) > other);
    }

    template <class T> bool vector<T>::operator >= (vector<T> & other) {
        return !((*this) < other);
    }

    // 
    // CONTAINER METHODS
    // --------------------

    // --------------------
    // ITERATOR CLASS
    // 

    template<class T> class Iterator : public std::iterator<std::bidirectional_iterator_tag, T> {
        friend class vector<T>;
        typedef Iterator <T> iterator;

    protected:
        T *begin;
        T *end;
        T *base;

    public:
        explicit Iterator (T *begin, T *end, T*base) : begin(begin), end(end), base(base) {}
        Iterator ( const iterator & it ) : begin(it.begin), end(it.end), base(it.base) {}
        Iterator ( iterator && it ) = default;

        bool operator != ( const iterator & other ) const;
        bool operator == ( const iterator & other ) const;
        T * operator -> () const;

        typename vector<T>::iterator & operator = ( const iterator & other );
        typename vector<T>::iterator & operator = ( iterator & other );
        typename vector<T>::iterator::reference operator * () const;

        typename vector<T>::iterator & operator ++ ();
        typename vector<T>::iterator   operator ++ ( int );
        typename vector<T>::iterator & operator -- ();
        typename vector<T>::iterator   operator -- ( int );

        typename vector<T>::iterator   operator +  ( int );
        typename vector<T>::iterator   operator -  ( int );
        int  operator +  ( iterator & other );
        int  operator -  ( iterator & other );
    };

    // -----------------------------
    // Forward iterator requirements
    // 

    template<class T> bool Iterator<T>::operator != (const iterator & other) const { 
        return this->base != other.base;
    }

    template<class T> bool Iterator<T>::operator == (const iterator & other) const  { 
        return this->base == other.base; 
    }

    template<class T> typename vector<T>::iterator::reference Iterator<T>::operator * () const { 
        return *base;
    }

    template<class T> T * Iterator<T>::operator -> () const  { 
        return this->base;
    }

    template<class T> typename vector<T>::iterator & Iterator<T>::operator ++ () {
        if (base != end) { base++; }
        return *this;
    }

    template<class T> typename vector<T>::iterator  Iterator<T>::operator ++ (int) {
        typename vector<T>::iterator ret_obj = *this;
        if (base != end) { base++; }
        return ret_obj;
    }

    template<class T> typename vector<T>::iterator & Iterator<T>::operator -- () {
        if (base != begin) { base--; }
        return *this;
    }

    template<class T> typename vector<T>::iterator  Iterator<T>::operator -- (int) {
        typename vector<T>::iterator ret_obj = *this;
        if (base != begin) { base--; }
        return ret_obj;
    }

    template<class T> typename vector<T>::iterator & Iterator<T>::operator = (iterator & other) { 
        this->base = other.base; 
        this->begin = other.begin;
        this->end = other.end;
        return *this;
    }

    template<class T> typename vector<T>::iterator & Iterator<T>::operator = (const iterator & other) {
        this->base = other.base; 
        this->begin = other.begin;
        this->end = other.end;
        return *this;
    }


    template<class T> typename vector<T>::iterator Iterator<T>::operator + (int x) { 
        if (base + x >= end) { base = end; }
        else { base = base + x; }
        return *this;
    }


    template<class T> typename vector<T>::iterator Iterator<T>::operator - (int x) { 
        if (base - x <= begin) { base = begin; }
        else { base = base - x; }
        return *this;
    }

    template<class T> int Iterator<T>::operator + (iterator & other) { 
        return base - begin + other.base  - begin;
    }


    template<class T> int Iterator<T>::operator - (iterator & other) { 
        return base - other.base;
    }

}