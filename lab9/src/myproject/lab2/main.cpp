#include "Vector.hpp"
#include <fstream>
#include <random>
#include <algorithm>

using namespace std;

/**
 * Создайте собственный класс Vector по образу и подобию std::vector. 
 * Класс должен иметьметоды begin, end, size,
 * методы push_back (с копированием и перемещением), pop_back, erase.
 * конструктор копирования, move-конструктор,
 * оператор присваивания с копированием и с перемещением,
 * метод swap внутри класса, через который реализуется перемещение,
 * функция swap вне класса для совместимости с STL алгоритмами.
 * Класс должен быть шаблоном, единственный аргумент которого является типом элемента контейнера. 
 * Класс не должен использовать другие классы STL (в особенности, класс std::vector).
*/
int main() {
    newns::vector<ofstream*> streams;
    const int numStreams = 10;

    for (int i = 0; i < numStreams; i++){
        ofstream *out = new std::ofstream;
        string fileName = to_string(i);
        out->open(fileName.c_str());
        streams.push_back(out);
    }

    auto it1 = streams.begin();
    
    random_device rd;
    shuffle(streams.begin(), streams.end(), rd);

    int i = 0;
    for (auto stream : streams) {
        cout << stream << endl;
        (*stream) << i; 
        stream->close();
        delete stream;
        i++;
    }

    return 0;
}