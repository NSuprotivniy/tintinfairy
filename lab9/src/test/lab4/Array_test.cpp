#include <gtest/gtest.h>
#include <array>
#include <myproject/lab4/Array.hpp>

struct array_param {
    std::array<std::array<int,2>,3> arr1;
    std::array<std::array<int,2>,3> arr2;
    std::array<std::array<int,2>,3> arr_add;
    std::array<std::array<int,2>,3> arr_diff;
    std::array<std::array<int,2>,3> arr_and;
    std::array<std::array<int,2>,3> arr_or;
    std::array<std::array<bool,2>,3> mask;
    int new_value;
    std::array<std::array<int,2>,3> expected_after_mask;
};



class Array_test: public ::testing::TestWithParam<array_param> {};

TEST_P(Array_test, _) {
    const array_param& param = GetParam();

    Array<int, 3, 2> arr1(param.arr1);
    Array<int, 3, 2> arr2(param.arr2);

    Array<int, 3, 2> arr_add = arr1 + arr2;
    Array<int, 3, 2> arr_diff = arr1 - arr2;
    Array<int, 3, 2> arr_and = arr1 & arr2;
    Array<int, 3, 2> arr_or = arr1 | arr2;

    Array<int, 3, 2> param_add(param.arr_add);
    Array<int, 3, 2> param_diff(param.arr_diff);
    Array<int, 3, 2> param_and(param.arr_and);
    Array<int, 3, 2> param_or(param.arr_or);

    Array<bool, 3, 2> mask(param.mask);
    MaskedArray<int, 3, 2> masked = arr1(mask);
    masked = param.new_value;
    Array<int, 3, 2> expected_after_mask(param.expected_after_mask);


    EXPECT_EQ(arr_add,  param_add);
    EXPECT_EQ(arr_diff, param_diff);
    EXPECT_EQ(arr_and, param_and);
    EXPECT_EQ(arr_or, param_or);
    EXPECT_EQ(arr1, expected_after_mask);
}

INSTANTIATE_TEST_CASE_P(
    _,
    Array_test,
    ::testing::Values(
        array_param{
            {{{1,2}, {3,4}, {5,6}}},
            {{{7,8}, {9,10}, {11,12}}},
            {{{8,10}, {12,14}, {16,18}}},
            {{{-6,-6}, {-6,-6}, {-6,-6}}},
            {{{1,0}, {1,0}, {1,4}}},
            {{{7,10}, {11,14}, {15,14}}},
            {{{true, true}, {false, true}, {false, false}}},
            5,
            {{{5, 5}, {3, 5}, {5, 6}}},
        }
    )
);