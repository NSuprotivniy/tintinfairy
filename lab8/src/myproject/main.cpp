#include <iostream>
#include <thread>
#include <algorithm>
#include <vector>
#include <cstdlib>
#include "MergeSort.hpp"
using namespace std;

int main()
{
	std::vector<int> vec1{ 9, 5, 4, 3, 2, 4, 5, 8, 0, 5, 3, 8, 2, 9 };
	//multithread sort
	std::cout << "Multithread sort" << std::endl;
	multi_merge_sort(vec1);
	print_vec(vec1);
	//lineal sort
	std::vector<int> vec{ 9, 5, 4, 3, 2, 4, 5, 8, 0, 5, 3, 8, 2, 9 };
	std::cout << "Lineal sort" << std::endl;
	lin_merge_sort(vec);
	print_vec(vec);
	//equality
	if(correct(vec, vec1)) std::cout <<"Multithreading is correct"<< std::endl;
	//work time
	//10

	for (int size = 10; size < 1000000000; size *= 10) {
		vector<int> x(size); 
		for (int i = 0; i < size; i++) {
			x[i] = rand();
		}

		cout << "Number of elements " << size << endl;
		cout << "Multithread sort working time (ms) " << threaded_work_time(x).count() << endl;
		cout << "Simple sort working time (ms) " << nonthreaded_work_time(x).count() << endl;
	}

	return 0;
}