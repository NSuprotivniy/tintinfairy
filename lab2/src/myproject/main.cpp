#include "Vector.hpp"
#include <fstream>
#include <random>
#include <algorithm>

using namespace std;

int main() {
    newns::vector<ofstream*> streams;
    const int numStreams = 10;

    for (int i = 0; i < numStreams; i++){
        ofstream *out = new std::ofstream;
        string fileName = to_string(i);
        out->open(fileName.c_str());
        streams.push_back(out);
    }

    auto it1 = streams.begin();
    
    random_device rd;
    shuffle(streams.begin(), streams.end(), rd);

    int i = 0;
    for (auto stream : streams) {
        cout << stream << endl;
        (*stream) << i; 
        stream->close();
        delete stream;
        i++;
    }

    return 0;
}