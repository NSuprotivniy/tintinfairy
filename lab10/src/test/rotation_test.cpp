#include <gtest/gtest.h>
#include <myproject/Rotation.hpp>

TEST(Rotation, OK)
{
    std::vector<double> vec({{3, 4}});
    std::vector<double> expected_90({{4, -3}});
    std::vector<double> expected_180({{-4, 3}});
    std::vector<double> expected_270({{-3, -4}});

    rotation(vec, 90);
    EXPECT_EQ(expected_90, vec);

    rotation(vec, 180);
    EXPECT_EQ(expected_180, vec);

    rotation(vec, 270);
    EXPECT_EQ(expected_270, vec);

    rotation(vec, 1);
    EXPECT_EQ(vec, vec);
}