#include "Rotation.hpp"
#include <cmath>
#include <iostream>
#include <string>
#include <vector>
#define PI 3.14159265  

void rotation(std::vector<double> &vec, int angle) {

	if (angle % 90 == 0) {
		if (angle % 360 == 90)
		{
			double t = vec[1];
			vec[1] = - vec[0];
			vec[0] = t;
		} 
		else if (angle % 360 == 180)
		{
			vec[0] = - vec[0];
			vec[1] = - vec[1];
		}
		else if (angle % 360 == 270)
		{
			double t = vec[0];
			vec[0] = - vec[1];
			vec[1] = t;
		}

		std::cout << "Rotated vector:" << vec[0] << vec[1] << std::endl;
		
	}
	else std::cout << "Angle doesn't multiples of 90 degrees!!!" << std::endl;
}