#include <iostream>
#include <string>
#include "Rotation.hpp"
#include <vector>
#include <cmath>


int main() {
	std::cout << "Input your vector coordinates:" << std::endl;
	int angle = 0;
	std::vector<double> vec(2);
	std::cout << "Input vector:" << std::endl;
	for (int j = 0; j < 2; j++)
		std::cin >> vec[j];

	std::cout << std::endl;
	std::cout << "Input an angle multiples of 90 degrees:" << std::endl;
	std::cin >> angle;
	std::cout << std::endl;
	rotation(vec, angle);

	return 0;
}